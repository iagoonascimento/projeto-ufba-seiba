require(["esri/Map","esri/layers/FeatureLayer","esri/views/MapView", "esri/PopupTemplate",
"esri/widgets/Editor", "esri/popup/content/AttachmentsContent", "esri/popup/content/TextContent"], 
function (Map,FeatureLayer, MapView, PopupTemplate, Editor, AttachmentsContent,
  TextContent) {
    let editor, features;
    const RenderermapaCalor = {
      type: "heatmap",
      colorStops: [
        { color: "rgba(0,0,0,0)", ratio: 0 },
        { color: "#90a1be", ratio: 0.083 },
        { color: "#9c8184", ratio: 0.167 },
        { color: "#a761aa", ratio: 0.249 },
        { color: "#af4980", ratio: 0.332 },
        { color: "#b83055", ratio: 0.415 },
        { color: "#c0182a", ratio: 0.498 },
        { color: "#c80000", ratio: 0.581 },
        { color: "#d33300", ratio: 0.664 },
        { color: "#de6600", ratio: 0.747 },
        { color: "#e99900", ratio: 0.83 },
        { color: "#f4cc00", ratio: 0.913 },
        { color: "#ffff00", ratio: 1 }
      ],
      maxPixelIntensity: 25,
      minPixelIntensity: 0
    };

    const colorVisVar1 ={
        "type": "color",
        "field": "pib",
        "valueExpression": null,
        "stops":[{
            "value": 0,
            "color":[
                255,
                252,
                212,
                255
            ],
            "label": "< 0"

            },
            {
              "value": 25,
              "color": [
                224,
                178,
                193,
                255
              ],
              "label": null
            },
            {
              "value": 50.8,
              "color": [
                193,
                104,
                173,
                255
              ],
              "label": "50.8"
            },
            {
              "value": 75.9,
              "color": [
                123,
                53,
                120,
                255
              ],
              "label": null
            },
            {
              "value": 101,
              "color": [
                53,
                2,
                66,
                255
              ],
              "label": "> 101"
            }
        ],
        "legendOptions":{
            title: "pib"
        }
    }

    const sizeVisVar1 = {
        "type": "size",
        "field": "pib",
        "normalizationField": "populacao",
        "valueExpression": null,
        "valueUnit": "unknown",
        "minSize": {
            "type": "size",
            "valueExpression": "$view.scale",
            "stops": [{
                "value": 3190,
                "size":10
              },
              {
                "value": 80000,
                "size": 10
              },
              {
                "value": 150000,
                "size": 10
              },
              {
                "value": 220000,
                "size": 10
              },
              {
                "value": 296000,
                "size": 10
              }
            ]
        },

        "maxSize": {
            "type": "size",
            "valueExpression": "$view.scale",
            "stops": [{
                "value": 3190,
                "size": 40
              },
              {
                "value": 80000,
                "size": 50
              },
              {
                "value": 150000,
                "size": 20
              },
              {
                "value": 220000,
                "size": 60
              },
              {
                "value": 296000,
                "size": 60
              }
            ]
          },
          "minDataValue": 0,
          "maxDataValue": 10000000000
    }

    const colorVisVar2 ={
      "type": "color",
      "field": "populacao",
      "valueExpression": null,
      "stops":[{
          "value": 40000,
          "color":[
              255,
              252,
              212,
              255
          ],
          "label": "< 0"

          },
          {
            "value": 100000,
            "color": [
              224,
              178,
              193,
              255
            ],
            "label": null
          },
          {
            "value": 500000,
            "color": [
              193,
              104,
              173,
              255
            ],
            "label": "50.8"
          },
          {
            "value": 1000000,
            "color": [
              123,
              53,
              120,
              255
            ],
            "label": null
          },
          {
            "value": 3000000,
            "color": [
              53,
              2,
              66,
              255
            ],
            "label": "> 101"
          }
      ],
      "legendOptions":{
          title: "pib"
      }
    }

  const sizeVisVar2 = {
      "type": "size",
      "field": "populacao",
      "normalizationField": "populacao",
      "valueExpression": null,
      "valueUnit": "unknown",
      "minSize": {
          "type": "size",
          "valueExpression": "$view.scale",
          "stops": [{
              "value": 3190,
              "size": 1
            },
            {
              "value": 80000,
              "size": 2
            },
            {
              "value": 150000,
              "size": 3
            },
            {
              "value": 220000,
              "size": 4
            },
            {
              "value": 296000,
              "size": 5
            }
          ]
      },

      "maxSize": {
          "type": "size",
          "valueExpression": "$view.scale",
          "stops": [{
              "value": 3190,
              "size": 40
            },
            {
              "value": 80000,
              "size": 30
            },
            {
              "value": 150000,
              "size": 20
            },
            {
              "value": 220000,
              "size": 10
            },
            {
              "value": 296000,
              "size": 1
            }
          ]
        },
        "minDataValue": 0,
        "maxDataValue": 10000000000
    }

    const RendererPib = {
        type: "simple",
        symbol: {
        type: "simple-marker",
        outline: {
                color: [128, 128, 128],
                width: 0.5
            }
        },
    label: "pib",
    visualVariables: [colorVisVar1, sizeVisVar1]
    }

    const RendererPop = {
      type: "simple",
      symbol: {
      type: "simple-marker",
      outline: {
              color: [128, 128, 128],
              width: 0.5
          }
      },
  label: "populacao",
  visualVariables: [colorVisVar2, sizeVisVar2]
    }

    const editThisAction = {
      title: "Editar",
      id: "edit-this",
      className: "esri-icon-edit"
    };

    const Templatepopup = new PopupTemplate({
      title: "Cidade de {municipio}",
      content: [{
        type: "fields",
        fieldInfos: [{
          fieldName: "populacao",
          visible: true,
          label: "População: "
        },
        {
          fieldName: "PIB",
          visible: true,
          label: "PIB: "
        },
        {
          fieldName: "IDHM",
          visible: true,
          label: "IDHM: "
        }
      ],
      
      }],actions: [editThisAction]

      //possui população de {populacao} de habitantes, idhm de {idh} e pib de {pib}

    })

    const IDHM = new FeatureLayer({
      url: "https://services5.arcgis.com/mSdKoiAwSEHO5XQb/arcgis/rest/services/idhm/FeatureServer",
      visible: true,
      renderer: RenderermapaCalor,
      opacity: 0.50
    });

    const PIB = new FeatureLayer({
      url: "https://services5.arcgis.com/mSdKoiAwSEHO5XQb/arcgis/rest/services/shape_selecao/FeatureServer",
      popupTemplate: Templatepopup,
      renderer: RendererPib
    });

    const populacao = new FeatureLayer({
      url: "https://services5.arcgis.com/mSdKoiAwSEHO5XQb/arcgis/rest/services/shape_selecao/FeatureServer",
      popupTemplate: Templatepopup,
      renderer: RendererPop
    });

    var map = new Map({
      basemap: "topo-vector"
    });

    var view = new MapView({
      container: "viewDiv",
      map: map,
      zoom: 8,
      center: [-38.5011, -12.9718],
      popup: {
        dockEnabled: true,
        dockOptions: {
          position: "bottom-right",
          buttonEnabled: false,
          breakpoint: false
        }
      }
    });

    view.when(function(){
      map.add(IDHM)
      map.add(PIB)
      map.add(populacao)
    });


    var botaoIDHM = document.querySelector("#IDHM");
    botaoIDHM.addEventListener("change", function() {
      
      IDHM.visible = botaoIDHM.checked;
    });

    var botaoPib = document.querySelector("#PIB");
    botaoPib.addEventListener("change", function() {
      
      PIB.visible = botaoPib.checked;
    });

    var botaoPopulacao = document.querySelector("#Populacao");
    botaoPopulacao.addEventListener("change", function() {
      
      populacao.visible = botaoPopulacao.checked;
    });

});